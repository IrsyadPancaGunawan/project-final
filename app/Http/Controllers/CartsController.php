<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CartsController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth')->except(['index']);
    // }

    public function create() {
        return view('carts.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:carts',
            'jumlah' => 'required',
            'total_pembayaran' => 'required',
        ]);

        $query = DB::table('carts')->insert([
            "nama" => $request["nama"],
            "jumlah" => $request["jumlah"],
            "total_pembayaran" => $request["total_pembayaran"],
        ]);

        return redirect('/carts')->with('success', 'Produk Berhasil Disimpan Ke Keranjang');

    }
    public function index(){
        $carts = DB::table('carts')->get();
        // dd($cast);
        return view('carts.index', compact('carts'));
    }

    public function show($id) {
        $cart = DB::table('carts')->where('id', $id)->first();
        // dd($cast);
        return view('carts.show', compact('cart'));
    }

    public function edit($id) {
        $cart = DB::table('carts')->where('id', $id)->first();
        return view('carts.edit', compact('cart'));
    }

    public function update($id, Request $request) {
        // $request->validate([
        //     'nama' => 'required|unique:products',
        //     'stok' => 'required',
        //     'deskripsi' => 'required',
        //     'image' => 'required',
        //     'harga' => 'required'
        // ]);

        $query = DB::table('carts')
                    ->where('id', $id)
                    ->update([
                        "nama" => $request["nama"],
                        "jumlah" => $request["jumlah"],
                        "total_pembayaran" => $request["total_pembayaran"],
                    ]);
        return redirect('/carts')->with('success', 'Berhasil update keranjang');
    }
    public function destroy($id) {
        $query = DB::table('carts')->where('id', $id)->delete();
        return redirect('/carts')->with('success', 'Produk di Keranjang berhasil dihapus');
    }
}
