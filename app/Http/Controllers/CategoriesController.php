<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CategoriesController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth')->except(['index']);
    // }

    public function create() {
        return view('categories.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:categories',
            'icon' => 'required',
        ]);

        $query = DB::table('categories')->insert([
            "nama" => $request["nama"],
            "icon" => $request["icon"],
        ]);

        return redirect('/categories')->with('success', 'Kategori Berhasil Disimpan');

    }
    public function index(){
        $categories = DB::table('categories')->get();
        // dd($cast);
        return view('categories.index', compact('categories'));
    }

    public function show($id) {
        $category = DB::table('categories')->where('id', $id)->first();
        // dd($cast);
        return view('categories.show', compact('category'));
    }

    public function edit($id) {
        $category = DB::table('categories')->where('id', $id)->first();
        return view('categories.edit', compact('category'));
    }

    public function update($id, Request $request) {
        // $request->validate([
        //     'nama' => 'required|unique:products',
        //     'stok' => 'required',
        //     'deskripsi' => 'required',
        //     'image' => 'required',
        //     'harga' => 'required'
        // ]);

        $query = DB::table('categories')
                    ->where('id', $id)
                    ->update([
                        "nama" => $request["nama"],
                        "icon" => $request["icon"],
                    ]);
        return redirect('/categories')->with('success', 'Berhasil update kategori');
    }
    public function destroy($id) {
        $query = DB::table('categories')->where('id', $id)->delete();
        return redirect('/categories')->with('success', 'Kategori berhasil dihapus');
    }
}
