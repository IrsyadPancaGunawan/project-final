<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  $categories = Category::all();
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $request->validate(
            [
                'category_name' => 'required',
                'image' => 'required'
            ]
        );

        $img = $request->file('image'); //mengambil file dari form
        $filename = time() . "-" . $img->getClientOriginalName(); //mengambil dan mengedit nama file dari form
        $img->move('img', $filename); //proses memasukkan image ke dalam direktori laravel


        Category::create(
            [
                // kiri nama tabel->nama form
                'category_name' => $request->category_name,
                'icon' => $filename,
            ]
        );

        return redirect('/category')->with('status', 'Berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('category.update', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        // return $request;
        $request->validate(
            [
                'category_name' => 'required',
                'image' => 'required',
            ]
        );

        if ($request->image != null) {
            $img = $request->file('image'); //mengambil dari form
            $filename = time() . "_" .$img->getClientOriginalName();
            $img->move('img', $filename);
            Category::where('id', $category->id)->update(
                [
                    'category_name' => $request->category_name,
                    'icon' => $filename,
                ]
            );
        } else {
            Category::where('id', $category->id)->update(
                [
                    'category' => $request->category_name,

                ]
            );
        }
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Category::destroy('id', $category->id);
        return redirect('/category');
    }
}
