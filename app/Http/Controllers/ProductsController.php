<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProductsController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth')->except(['index']);
    // }

    public function create() {
        return view('products.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:products',
            'stok' => 'required',
            'deskripsi' => 'required',
            'image' => 'required',
            'harga' => 'required'
        ]);

        $query = DB::table('products')->insert([
            "nama" => $request["nama"],
            "stok" => $request["stok"],
            "deskripsi" => $request["deskripsi"],
            "image" => $request["image"],
            "harga" => $request["harga"],
            "categories_id" => $request["categories_id"],
            "transactions_id" => $request["transactions_id"],
        ]);

        return redirect('/products')->with('success', 'Produk Berhasil Disimpan');

    }
    public function index(){
        $products = DB::table('products')->get();
        // dd($cast);
        return view('products.index', compact('products'));
    }

    public function show($id) {
        $product = DB::table('products')->where('id', $id)->first();
        // dd($cast);
        return view('products.show', compact('product'));
    }

    public function edit($id) {
        $product = DB::table('products')->where('id', $id)->first();
        return view('products.edit', compact('product'));
    }

    public function update($id, Request $request) {
        // $request->validate([
        //     'nama' => 'required|unique:products',
        //     'stok' => 'required',
        //     'deskripsi' => 'required',
        //     'image' => 'required',
        //     'harga' => 'required'
        // ]);

        $query = DB::table('products')
                    ->where('id', $id)
                    ->update([
                        "nama" => $request["nama"],
                        "stok" => $request["stok"],
                        "deskripsi" => $request["deskripsi"],
                        "image" => $request["image"],
                        "harga" => $request["harga"],
                        "categories_id" => $request["categories_id"],
                        "transactions_id" => $request["transactions_id"],
                    ]);
        return redirect('/products')->with('success', 'Berhasil update produk');
    }
    public function destroy($id) {
        $query = DB::table('products')->where('id', $id)->delete();
        return redirect('/products')->with('success', 'Produk berhasil dihapus');
    }
}
