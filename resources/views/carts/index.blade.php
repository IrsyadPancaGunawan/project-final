@extends('template.main')

@section('content')
<div class="ml-3 mt-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Carts Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <a class="btn btn-primary mb-2" href="/carts/create">Create New Carts</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama Produk</th>
                <th>Jumlah Produk</th>
                <th>Total Pembayaran</th>
                <th style="width: 40px">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($carts as $key => $cart)
                  <tr>
                      <td>{{ $key +1 }} </td>
                      <td>{{ $cart->nama }}</td>
                      <td>{{ $cart->jumlah }}</td>
                      <td>{{ $cart->total_pembayaran }}</td>
                      <td style="display: flex"> 
                          <a href="/carts/{{$cart->id}}" class="btn btn-info btn-sm">Show</a>
                          <a href="/carts/{{$cart->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                          <form action="/carts/{{$cart->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                          </form>
                      </td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" align="center">No Kategori</td>
                  </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
          </ul>
        </div> --}}
      </div>
</div>

<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endsection