@extends('template.main')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Category</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/categories" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama Kategori">
              @error('nama')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="icon">Icon</label>
                <input type="file" class="form-control" id="icon" name="icon" placeholder="Icon">
                @error('icon')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
</div>
@endsection