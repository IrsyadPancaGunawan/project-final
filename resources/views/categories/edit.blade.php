@extends('template.main')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Edit Categories {{$category->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/categories/{{$category->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama Produk" value="{{ old('nama', $category->nama)}}">
                  @error('nama')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                </div>
                <div class="form-group">
                    <label for="icon">Icon</label>
                    <input type="string" class="form-control" id="icon" name="icon" placeholder="Icon" value="{{ old('icon', $category->icon)}}">
                    @error('icon')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                </div>
              </div>
              <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
</div>

@endsection