@extends('template.main')
@section('title', 'Create Category')


@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height mt-5">
            <h3 class="ml-2"><b>Tambah Category</b></h3>
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form form-horizontal" action="{{ url('/category') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-2 mt-3">
                                            <label for="category_name">Category Name</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder=" Masukkan Judul"
                                                    id="category_name" name="category_name">
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="icon">Icon</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="file" class="form-control" placeholder=" Masukkan Judul"
                                                    id="icon" name="image">
                                            </div>
                                        </div>


                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Kirim</button>
                                            <a href="{{ url('/category') }}" class="btn btn-primary me-1 mb-1"
                                                role="button">
                                                Batal
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
