@extends('template.main')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Products</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/products" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="string" class="form-control" id="nama" name="nama" placeholder="Nama Produk">
              @error('nama')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="stok">Stok</label>
                <input type="integer" class="form-control" id="stok" name="stok" placeholder="Stok">
                @error('stok')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <input type="longText" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi">
                @error('deskripsi')
                  <div class="alert alert-danger">{{ $message}}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="image">Image</label>
              <input type="file" class="form-control" id="image" name="image" placeholder="Image">
              @error('image')
                <div class="alert alert-danger">{{ $message}}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="harga">Harga</label>
            <input type="integer" class="form-control" id="harga" name="harga" placeholder="Harga">
            @error('harga')
              <div class="alert alert-danger">{{ $message}}</div>
          @enderror
        </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
</div>
@endsection