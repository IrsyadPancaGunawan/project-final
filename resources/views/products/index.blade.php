@extends('template.main')

@section('content')
<div class="ml-3 mt-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Products Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <a class="btn btn-primary mb-2" href="/products/create">Create New Products</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama Produk</th>
                <th>Stok</th>
                <th>Deskripsi</th>
                <th>Image</th>
                <th>Harga</th>
                <th style="width: 40px">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($products as $key => $product)
                  <tr>
                      <td>{{ $key +1 }} </td>
                      <td>{{ $product->nama }}</td>
                      <td>{{ $product->stok }}</td>
                      <td>{{ $product->deskripsi }}</td>
                      <td>{{ $product->image }}</td>
                      <td>{{ $product->harga }}</td>
                      <td style="display: flex"> 
                          <a href="/products/{{$product->id}}" class="btn btn-info btn-sm">Show</a>
                          <a href="/products/{{$product->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                          <form action="/products/{{$product->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                          </form>
                      </td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" align="center">No Products</td>
                  </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
          </ul>
        </div> --}}
      </div>
</div>

<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endsection