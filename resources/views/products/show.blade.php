@extends('template.main')

@section('content')
    <div class="mt-3 ml-3">
        <h4> {{ $product->nama }}</h4>
        <p> {{ $product->stok }}</p>
        <p> {{ $product->deskripsi }}</p>
        <p> {{ $product->image }}</p>
        <p> {{ $product->harga }}</p>
    </div>
    
@endsection