<!DOCTYPE html>
<html lang="en">
<head>
    @include('template.header')

    <title>@yield('title')</title>
</head>
<body class="dashboard dashboard_1">
    <div class="full_container">
        @include('template.sidebar')
    </div>
    

    @include('template.footer')
</body>
</html>
